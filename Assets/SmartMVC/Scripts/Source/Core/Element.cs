﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SmartMVC
{
    /// <summary>
    /// Extension of the element class to handle different BaseApplication types.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Element<T> : Element where T : BaseApplication
    {
        /// <summary>
        /// Returns app as a custom 'T' type.
        /// </summary>
        new public T app { get { return (T)base.app; } }
    }

    /// <summary>
    /// Base class for all MVC related classes.
    /// </summary>
    public class Element : MonoBehaviour
    {

        /// <summary>
        /// Reference to the root application of the scene.
        /// </summary>
        public BaseApplication app { get { return m_app = Assert<BaseApplication>(m_app, true); } }
        private BaseApplication m_app;

        /// <summary>
        /// Finds a instance of 'T' if 'var' is null. Returns 'var' otherwise.
        /// If 'global' is 'true' searches in all scope, otherwise, searches in childrens.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p_var"></param>
        /// <param name="p_global"></param>
        /// <returns></returns>
        public T Assert<T>(T p_var, bool p_global=false) where T : Object { return p_var == null ? (p_global ? GameObject.FindObjectOfType<T>() : transform.GetComponentInChildren<T>(true)) : p_var; }

        /// <summary>
        /// Finds a instance of 'T' locally if 'var' is null. Returns 'var' otherwise.        
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p_var"></param>
        /// <returns></returns>
        public T AssertLocal<T>(T p_var) where T : Object { return p_var == null ? (p_var = GetComponent<T>()) : p_var; }

        /// <summary>
        /// Helper method for casting.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Cast<T>() { return (T)(object)this; }

        /// <summary>
        /// Searchs for a given element in the dot separated path.
        /// </summary>
        /// <param name="p_path"></param>
        /// <returns></returns>
        public T Find<T>(string p_path) where T : Component
        {
            List<string> tks = new List<string>(p_path.Split('.'));
            if (tks.Count <= 0) return default(T);
            Transform it = transform;
            while (tks.Count > 0)
            {
                string p = tks[0];
                tks.RemoveAt(0);
                it = it.Find(p);
                if (it == null) return default(T);
            }
            return it.GetComponent<T>();

        }

        /// <summary>
        /// Notifies to the listening controllers the event
        /// </summary>
        /// <param name="eventID">The name of the event to notify</param>
        /// <param name="data">The parameters to pass to the listening controllers</param>
        public void Notify(int eventID, params object[] data)
        {
            app.Notify(eventID, this, data);
        }

        /// <summary>
        /// Attach a method to an event in the dictionary
        /// </summary>
        /// <param name="eventID">The name of the event to attach</param>
        /// <param name="method">The method to detach</param>
        public void ListenToEvent(int eventID, BaseApplication.OnNotificationSentHandler method)
        {
            app.ListenToEvent(eventID, method, this);
        }

        /// <summary>
        /// Detach a method from an event in the dictionary
        /// </summary>
        /// <param name="eventID">The name of the event to detach</param>
        /// <param name="method">The method to detach</param>
        public void StopListenToEvent(int eventID, BaseApplication.OnNotificationSentHandler method)
        {
            if (!app) { return; }
            app.StopListenToEvent(eventID, method, this);
        }
    }
}