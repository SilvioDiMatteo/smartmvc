﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SmartMVC
{
    /// <summary>
    /// Contains all the events of the SmartMVC system
    /// </summary>
    public class Events
    {
        public const int SAVE_LEVEL = 0;
        public const int LOAD_LEVEL = 1;
    }
}
