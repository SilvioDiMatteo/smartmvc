﻿using UnityEngine;
using System.Collections;

namespace SmartMVC
{

    /// <summary>
    /// Base class for all Controllers in the application.
    /// </summary>
    public class Controller : Element { }

    /// <summary>
    /// Base class for all Controller related classes.
    /// </summary>
    public class Controller<T> : Controller where T : BaseApplication
    {
        /// <summary>
        /// Returns app as a custom 'T' type.
        /// </summary>
        new public T app { get { return (T)base.app; } }
    }
}