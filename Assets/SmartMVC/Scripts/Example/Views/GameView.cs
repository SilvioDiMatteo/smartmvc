﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartMVC;

public class GameView : View<GameApplication>
{
    public Text lblOutput;

    public void OnClickSaveLevel()
    {
        Notify(Events.SAVE_LEVEL, "Level Saved");
    }

    public void OnClickLoadLevel()
    {
        Notify(Events.LOAD_LEVEL, "Level Loaded");
    }
}
