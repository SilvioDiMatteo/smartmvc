﻿using UnityEngine;
using System.Collections;
using SmartMVC;

public class GameController : Controller<GameApplication>
{
    void Start()
    {
        ListenToEvent(Events.SAVE_LEVEL, SaveLevel);
        ListenToEvent(Events.LOAD_LEVEL, LoadLevel);
    }

    void OnDestroy()
    {
        StopListenToEvent(Events.SAVE_LEVEL, SaveLevel);
        StopListenToEvent(Events.LOAD_LEVEL, LoadLevel);
    }

    /// <summary>
    /// Print a message to the output label when the the level is saved
    /// </summary>
    /// <param name="data"></param>
    void SaveLevel(params object[] data)
    {
        string message = (string)data[0];
        app.view.lblOutput.text = message;
    }

    /// <summary>
    /// Print a message to the output label when the the level is loaded
    /// </summary>
    /// <param name="data"></param>
    void LoadLevel(params object[] data)
    {
        string message = (string)data[0];
        app.view.lblOutput.text = message;
    }
}
